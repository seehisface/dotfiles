#!/bin/bash
cd ~/.dotfiles
mv .git/config ~/gitconfigtmp 
rm -rf .git
git init
git add .
git commit --author="kauld <push@kauld.dev>" -m "Initial commit...."
rm .git/config
mv ~/gitconfigtmp .git/config
git push --force --mirror
